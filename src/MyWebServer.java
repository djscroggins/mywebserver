/*--------------------------------------------------------

1. Name / Date:
    David Scroggins
    January 27, 2018

2. Java version used, if not the official version for the class:

    1.8.0_72

3. Precise command-line compilation examples / instructions:

    > javac MyWebServer.java

4. Precise examples / instructions to run this program:

    In shell window:

    > java MyWebServer

    Open browser (tested in Firefox 58.0) and navigate to "localhost:2540" to display index page

    Hyperlinks are fully navigable (except for forbidden files such as .java or .class files)

5. List of files needed for running the program.

    MyWebServer.java

5. Notes:


----------------------------------------------------------*/

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;

public class MyWebServer {


    public static void main (String[] args) throws IOException {

        int queueLength = 6;
        int port = 2540;
        Socket sock;

            try (ServerSocket servsock = new ServerSocket(port, queueLength);
            ) {
                System.out.println("David Scroggins' MyWebServer 1.8 starting up ...");

                System.out.println("Listening for clients at port " + port);

                while (true) {
                    // Wait for connection request
                    // Upon successful connection returns new Socket object bound to specified port
                    sock = servsock.accept();
                    // Starts thread, passes Socket object and call run method of ClientWorker
                    new ClientWorker(sock).start();
                }
            } catch (SocketException se) {
                System.out.println("Socket shut down.");
            }
    }
}

// Declares ClientWorker subclass of Thread to process client requests
class ClientWorker extends Thread {

    private Socket sock;

    ClientWorker(Socket socketIn) {
        sock = socketIn;
    }

    public void run() {
        try (
                // Get socket OutputStream and open PrintStream on it so data can be sent to client
                PrintStream toClient = new PrintStream(sock.getOutputStream());
                // Get socket InputStream and open Scanner on it so client's input can be read
                // and written
                BufferedReader fromClient = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        ) {
            try {

                String request = fromClient.readLine();

                System.out.println("\nClient request:\n" + request);

                handleRequest(request, toClient);

            } catch (IllegalStateException ise) {

                System.out.println("Server read error");
                ise.printStackTrace();

            } catch (FileNotFoundException fnfe){
                // If file not found returns proper MIME header and displays simple html message to client
                System.out.println("File not found");
                String fileNotFoundPage = "<h1>404 Not Found</h1><h3>File not found</h3>";
                int pageLength = fileNotFoundPage.length();
                String responseHeader = "HTTP/1.1 404 Not Found " + " Content-Length: " + pageLength + " Content-Type: text/html" + "\r\n\r\n";
                toClient.println(responseHeader + fileNotFoundPage);
                System.out.println("\nServer Response:\n" + responseHeader + fileNotFoundPage);

            } catch (IllegalArgumentException iae) {
                // Requests for files other than .txt or .html or directories are forbidden and return proper MIME headers and
                // display simple html message to client
                System.out.println("This request not supported");
                String forbiddenResponsePage = "<h1>403 Forbidden</h1><h3>You are not allowed to access this file.</h3>";
                int pageLength = forbiddenResponsePage.length();
                String responseHeader = "HTTP/1.1 403 Forbidden " + " Content-Length: " + pageLength + " Content-Type: text/html" + "\r\n\r\n";
                toClient.println(responseHeader + forbiddenResponsePage);
                System.out.println("\nServer Response:\n" + responseHeader + forbiddenResponsePage);
            }

        } catch (IOException ioe) {
            System.out.println("There was an error communicating with the Client.");
            ioe.printStackTrace();
        }
    }

    /**
     * Method handles requests from client. Current functionality processes only GET requests.
     * @param requestIn first line of request from client
     * @param toClientIn PrintStream to client
     * @throws FileNotFoundException if client attempts to navigate to directory above root directory or requests file that
     * does not exist
     * @throws IllegalArgumentException if client attempts access file type that is not allowed in handlers
     */
    private void handleRequest(String requestIn, PrintStream toClientIn) throws FileNotFoundException, IllegalArgumentException {

        // Split request on spaces; first line should be HTTP method;
        // second line should be request content (file name or directory);
        // third line should be transfer protocol
        String[] requestArray = requestIn.split(" ");

        // If request is GET method, then process request
        if (requestArray[0].equals("GET")) {

            // Prevent directory traversal
            if (requestArray[1].equals("..") || requestArray[1].equals(":") || requestArray[1].equals("|")) {
                throw new FileNotFoundException();

            // Directory request
            } else if (requestArray[1].endsWith(".ico")) {
                System.out.println("Ignoring favicon request.");
            } else if (requestArray[1].endsWith("/")) {

                getFileStructure(requestArray[1], toClientIn);

            // fake-cgi request
            } else if (requestArray[1].contains("fake-cgi?")){

                addNums(requestArray[1], toClientIn);

            // Assume file request (handled inside logic for called method)
            } else {

                getFile(requestArray[1], toClientIn);
            }

        }
    }

    /**
     * Method determines if request is for index or subdirectory then calls method to build proper
     * html page
     * @param directoryIn requested directory
     * @param toClientIn PrintStream to client
     */

    private void getFileStructure (String directoryIn, PrintStream toClientIn) {

        // Index request
        if (directoryIn.equals("/")) {

            buildDirectoryPage(directoryIn, true, toClientIn);

        // Request for sub-directory
        } else {

            buildDirectoryPage(directoryIn, false, toClientIn);
        }
    }

    /**
     * Method builds html page to display current directory tree.
     * If at index level, adds header indicating so. If not at index level, provides home
     * button to return to index
     * @param directoryIn requested directory
     * @param isIndexIn boolean to specify whether page being built is index
     * @param toClientIn PrintStream to client
     */
    private void buildDirectoryPage (String directoryIn, boolean isIndexIn, PrintStream toClientIn) {

        // This might seem overkill since I know what the content type is
        // but I want to keep handlers for content type centralized in a single method
        String contentType = getContentType(directoryIn);

        // StringBuilder to contain constructed page
        StringBuilder htmlPage = new StringBuilder();

        // Add header if index page
        if (isIndexIn) {

            htmlPage.append("<h2>Index of WebServer Project</h2>\n");

        // Add home link to navigate to index for all other pages
        } else {

            htmlPage.append("<h3><a href=\"/\"> Home </a></h3>\n");

        }

        // Get requested directory as new File instance
        File requestedDirectory = new File("." + directoryIn);

        // List contents of current directory File instance
        File[] directoryContentsArray = requestedDirectory.listFiles();

        // Build hyperlink for contents; directory hrefs and add "/" to allow GET requests to traverse directory and
        // for display purposes
        for (File file : directoryContentsArray) {
            if (file.isDirectory()) {
                htmlPage.append("<a href=\"" + file.getName() + "/" + "\">" + file.getName() + "/" + "</a> <br>\n");
            } else if (file.isFile()) {
                htmlPage.append("<a href=\"" + file.getName() + "\">" + file.getName() + "</a> <br>\n");
            }
        }

        // Get length of characters; since plain text ASCII character count is equivalent to number of bytes
        int pageLength = htmlPage.length();

        // MIME response header to be sent to client
        String responseHeader = "HTTP/1.1 200 OK " + " Content-Length: " + pageLength + " Content-Type: " + contentType + "\r\n\r\n";
        // Send header and html to client
        toClientIn.println(responseHeader + htmlPage);

        System.out.println("\nServer response: \n" + responseHeader + htmlPage);
    }


    /**
     * Method processes a fake cig request to simulate back end processing.
     * Takes input name and two numbers, adds the numbers and computes the sum.
     * @param requestIn request from client
     * @param toClientIn PrintStream to client
     */
    private void addNums (String requestIn, PrintStream toClientIn) {

        String contentType = getContentType(requestIn);

        // Splits fake-cgi request string to access key/value pairs from form
        String[] fieldsArray = requestIn.split("\\?|&|person=|num1=|num2=");

        // Thanks to my recent experience with Scala in CSC 424 for even thinking to check on cleaning the array like this
        // Removes empty strings from array
        String[] trimmedFieldsArray = Arrays.stream(fieldsArray).filter(str -> !str.isEmpty()).toArray(String[]::new);

        // Parse relevant input strings to ints then add
        int num1 = Integer.parseInt(trimmedFieldsArray[2]);
        int num2 = Integer.parseInt(trimmedFieldsArray[3]);
        int sum = num1 + num2;

        buildAddNumsPage(num1, num2, sum, trimmedFieldsArray[1], contentType, toClientIn);

    }

    /**
     * Method builds html response page to client form submit
     * @param num1In number inputted by client
     * @param num2In number inputted by client
     * @param sumIn sum of num1 and num2
     * @param nameIn name inputted by client
     * @param contentTypeIn content type of request
     * @param toClientIn PrintStream to client
     */
    private void buildAddNumsPage (int num1In, int num2In, int sumIn, String nameIn, String contentTypeIn, PrintStream toClientIn) {

        // StringBuilder to hold html page
        StringBuilder htmlPage = new StringBuilder();

        // Add home button to navigate back to index
        htmlPage.append("<a href=\"/\"> Home </a> <br> <br>\n");

        htmlPage.append("<h3>Hi " + nameIn + ", the sum of " + num1In + " and " + num2In + " is " + sumIn + "</h3>\n");

        // Get number of characters; since plain text ASCII character count is equivalent to number of bytes
        int pageLength = htmlPage.length();

        // MIME response header to be sent to client
        String responseHeader = "HTTP/1.1 200 OK" + " Content-Length: " + pageLength + " Content-Type: " + contentTypeIn + "\r\n\r\n";
        // Send header and html page to client
        toClientIn.println(responseHeader + htmlPage);

        System.out.println("\nServer Response: \n" + responseHeader + htmlPage);
    }


    /**
     * Method retrieves and reads file contents to client for .txt and .html extensions
     * @param fileNameIn request from client
     * @param toClientIn PrintStream to client
     * @throws IllegalArgumentException if client attempts to access file type that is not allowed in handlers
     * @throws FileNotFoundException if file is not found in current directory
     */
    private void getFile (String fileNameIn, PrintStream toClientIn) throws IllegalArgumentException, FileNotFoundException {

        // Remove leading "/" so relative path can be used
        if(fileNameIn.startsWith("/")) {
            fileNameIn = fileNameIn.substring(1);
        };

        // Opens actual file and connects to it with InputStream
        try (
            InputStream fileStream = (new FileInputStream(fileNameIn));
        ) {

            File file = new File(fileNameIn);

            String contentType = getContentType(fileNameIn);

            // File.length() returns long; this method safely parses to int
            int fileLength = getFileLength(file);

            // MIME header string to be sent to client
            String responseHeader = "HTTP/1.1 200 OK " + " Content-Length: " + fileLength + " Content-Type: " + contentType + "\r\n\r\n";


            byte[] fileWriteArray = new byte[fileLength];
            // Reads bytes from InputStream (i.e. contents of file) to fileWriteArray
            fileStream.read(fileWriteArray);

            // Send header to client
            toClientIn.println(responseHeader);
            // Writes fileLength bytes from fileWriteArray (now holds contents of file) with zero offset to PrintStream to client.
            toClientIn.write(fileWriteArray, 0, fileLength);

            System.out.println("\nServer response:\n" + responseHeader);
            System.out.write(fileWriteArray, 0, fileLength);

        } catch (IOException ioe) {
            System.out.println("Input/Output exception");
            // Indicates file was not found; throws FileNotFoundException which is handled in worker's run method
            throw new FileNotFoundException();
        }

    }

    /**
     * Handler method to determine request Content-Type
     * @param requestIn request from client
     * @return appropriate Content-Type
     * @throws IllegalArgumentException if client attempts to access file type that is not allowed in handlers
     */
    private String getContentType (String requestIn) throws IllegalArgumentException {
        if (requestIn.endsWith(".txt")) {

            return "text/plain";

        } else if (requestIn.endsWith(".html")) {

            return "text/html";

        } else if (requestIn.endsWith("/")) {

            return "text/html";

        } else if (requestIn.contains("fake-cgi?")) {

            return "text/html";

        }
        // Indicates that this GET request is not supported or allowed; handled in worker's run method
        else {

            throw new IllegalArgumentException();
        }
    }

    /**
     * Method to safely cast File.length() return value [long] to int
     * @param fileIn open File
     * @return length of file as int
     */
    private int getFileLength (File fileIn) {

        try {
            long fileLength = fileIn.length();
            return Math.toIntExact(fileLength);
        } catch (ArithmeticException ae) {
            return 10000; // Can accommodate plain text up to 10000 characters
        }
    }
}